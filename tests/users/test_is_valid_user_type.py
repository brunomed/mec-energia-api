import pytest
from utils.user.user_type_util import UserType
from users import models 
from datetime import date

from users.models import UniversityUser
from users.models import CustomUser
from universities.models import ConsumerUnit, University

from tests.test_utils import dicts_test_utils
from tests.test_utils import create_objects_test_utils

@pytest.mark.django_db
class TestUserTypeIsValidUserType:
    def setup_method(self):
        self.university_dict = dicts_test_utils.university_dict_1
        self.user_dict = dicts_test_utils.university_user_dict_1

        self.university = create_objects_test_utils.create_test_university(self.university_dict)
        self.user = create_objects_test_utils.create_test_university_user(self.user_dict, self.university)
        self.super_user_dict = dicts_test_utils.super_user_dict_1
        self.super_user = create_objects_test_utils.create_test_super_user(self.super_user_dict)


        self.consumer_units = []
        for i in range(3):
            self.consumer_units.append(ConsumerUnit(
                id=i+1,
                name=f'UC {i+1}',
                code=f'{i+1}',
                university=self.university,
                is_active=True,
                created_on=date.today()
            ))
        ConsumerUnit.objects.bulk_create(self.consumer_units)

    def test_is_valid_user_type_invalid_all_users(self):
        with pytest.raises(Exception) as error:
            CustomUser.objects.create(
            first_name= 'John',
            last_name= 'Doe',
            email= 'john.doe@example.com',
            type= 'Abacateiro',
        )
        assert str(error.value) == 'Error Create User: User type (Abacateiro) does not exist'
            

    def test_is_valid_user_type_university_user(self):
        with pytest.raises(Exception) as error:
            UniversityUser.objects.create(
            first_name= 'John',
            last_name= 'Doe',
            email= 'john.doe@example.com',
            type= 'super_user',
            university= self.university,
        )
        assert str(error.value) == "Error Create User: Wrong User type (super_user) for this Model User (<class 'users.models.UniversityUser'>)"

    def test_is_valid_user_type_custom_user(self):
        with pytest.raises(Exception) as error:
            CustomUser.objects.create(
            first_name= 'John',
            last_name= 'Doe',
            email= 'john.doe@example.com',
            type= 'university_admin',
        )
        assert str(error.value) == "Error Create User: Wrong User type (university_admin) for this Model User (<class 'users.models.CustomUser'>)"
    
    def test_is_valid_user_type_valid_all_users(self):
        user_type = UserType.is_valid_user_type('super_user')
        assert user_type == 'super_user'